const {Telegraf} = require('telegraf')

const dictionary = require('./dictionary')

const messageParser = (ctx) => {
    if(ctx.message.chat.id !== parseInt(process.env.CHAT_ID)){
        return false
    }
    if(typeof ctx.message.new_chat_member === 'object'){
        ctx.replyWithSticker('CAACAgIAAxkBAAEC6kRhQ6F7zWuETkRUPpAdBsVD4DryyQACeBAAAn9cOUmHykUzbtrOiiAE')
    }
    dictionary.prostitute.some(item => {
        if(ctx.message.text.toLowerCase().indexOf(item) !== -1){
            ctx.replyWithSticker('CAACAgIAAxkBAAEC6kphQ6bkp_d9L4zfvOKZ5xY8GiI7mQACHBMAAvQcOUl6_ARlekOiASAE')
        }
    });
    dictionary.arta.some(item => {
        if(ctx.message.text.toLowerCase().indexOf(item) !== -1){
            ctx.replyWithSticker('CAACAgIAAxkBAAEC6kxhQ6dLxtORkIIg7EnDgIU6LdnTdQACnBAAArw2MEmXEdUAAay_NRYgBA')
        }
    });
    dictionary.otkrutka.some(item => {
        if(ctx.message.text.toLowerCase().indexOf(item) !== -1){
            ctx.replyWithSticker('CAACAgIAAxkBAAEC6k5hQ6dw38rGDftUOc659k75O3UiwAAC3Q0AAk5cOUleiRJAmBgtdSAE')
        }
    });
}

module.exports = messageParser