const { Telegraf } = require('telegraf')
const messageParser = require('./modules/messageParser')

const bot = new Telegraf(process.env.BOT_ID)

bot.start((ctx) => hello(ctx))
bot.on('message', (ctx) => messageParser(ctx))
bot.launch()

process.once('SIGINT', () => bot.stop('SIGINT'))
process.once('SIGTERM', () => bot.stop('SIGTERM'))